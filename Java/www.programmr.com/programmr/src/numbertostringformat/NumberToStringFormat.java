package numbertostringformat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Complete the following program which takes input as a number and converts it into string format.
Initially arrays of string are given just use it for your logic.
Scenario will be:
Enter the number:
54
Entered number is:
fifty four
 */

public class NumberToStringFormat {
    public static String ones[]={"one","two","three","four","five","six"," seven", "eight","nine","ten","eleven",
            "twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
    public static String tens[]={"twenty","thirty","fourty","fifty","sixty","seventy","eighty","ninety"};

    public static void main(String[] args) throws IOException {
        System.out.println("Enter value: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        System.out.println(numToStr(num));
    }

    private static String numToStr(int num) {
        int ten = (num - num % 10) / 10;
        if ((num > 0) && (num < 20)) {
            return new String(ones[num - 1]);
        } else {
            if ((num % 10) == 0) return new String(tens[ten - 2]);
            return new String(tens[ten - 2] + " " + ones[(num % 10) - 1]);
        }
    }
}
