package binaryOrNot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
A binary number is written as combination of 1's and 0's.
Binary number 100 indicates 4.

Write a program which accepts a number and prints whether it is binary or not.

For Ex:

1) if user inputs 1040400
then it should print "Not Binary".

2) if user inputs 1000100
then it should print "Binary".
 */
public class BinaryOrNot {
    public static void main(String[] args) throws IOException {
        System.out.println("Enter value: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());

        System.out.println(binOrNot(num));
    }

    public static boolean binOrNot(int value){
        boolean answer = true;
        String temp = Integer.toString(value);
        int [] newArray = new int[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            newArray[i] = temp.charAt(i) - '0';
            if ((newArray[i] == 0) || (newArray[i] == 1)){}
                else answer = false;
        }
        return answer;
    }
}
