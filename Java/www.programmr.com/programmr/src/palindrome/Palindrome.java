package palindrome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Write a program which takes input as string and check if it is palindrome or not.
If the string and the reverse of string are same then we say string is palindrome.
If string is "level" then the output should be: palindrome

If string is "helloworld" then the output should be: not a palindrome
 */
public class Palindrome {
    public static void main(String[] args) throws IOException {

        System.out.println("Enter the string:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String strn=br.readLine();
        System.out.println("Result string is: " + invertLine(strn));

        if(strn.equals(invertLine(strn))){
            System.out.println("palindrome");
        } else System.out.println("not palindrome");

    }
    public static String invertLine(String line){
        char [] arrFromStr = line.toCharArray();
        char temp;
        for (int i = 0; i < arrFromStr.length/2; i++){
            temp = arrFromStr[i];
            arrFromStr[i] = arrFromStr[arrFromStr.length - i - 1];
            arrFromStr[arrFromStr.length - i - 1] = temp;
        }
        return new String(arrFromStr);
    }
}